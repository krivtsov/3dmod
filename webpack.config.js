const NODE_ENV = process.env.NODE_ENV;
const webpack = require('webpack');
const path = require("path");

module.exports = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        app: path.join(__dirname, 'src/app/app.module.js'),
    },
    output: {
        path: path.join(__dirname, '/src/dist'),
        filename: '[name].build.js',
        library: 'app'
    },
    watch: NODE_ENV == 'development',
    watchOptions: {
        aggregateTimeout: 100
    },

    devtool: NODE_ENV == 'development' ? 'cheap-source-map' : null,

    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js']
    },

    resolveLoader: {
        modulesDirectories: ['node_modules'],
        moduleTemplates: ['*-loader', '*'],
        extensions: ['', '.js']
    },

    plugins: [
        new webpack.NoErrorsPlugin(), // if error mudule do not compiling
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV),
            LANG: JSON.stringify('ru')
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common',
         //   chunks: ['app.module.js']
        }),
        // new webpack.optimize.UglifyJsPlugin({
        //     compress: {
        //         warnings: false,
        //         drop_console: true,
        //         unsafe: true
        //     }
        // })
    ],
    module: {
        loaders: [{
            test: /\.js$/, 
            loader: 'babel-loader',
            exclude: /node_modules/,
            include: [
                path.join(__dirname, "/src/app/"),
            ],
            query: {
                presets: ['es2015', 'react']
            }

        }]
    }
};


if (NODE_ENV == 'production') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                ///  drop_console: true,
                unsafe: true
            }
        })
    )
}
var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    stripe = require("stripe")("sk_test_2HxNOsZTKZA0mASJQL44KXYE");
var count = 0;
const port = 3000;

app.use(express.static(__dirname + '/../src'));
app.use(express.static(__dirname + '/../node_modules'));
app.use(express.static(__dirname + '/../src/app/template/'));
app.use(express.static(__dirname + '/../'));


// routes  =============================================

// api

app.get('/api/news', function (req, res) {
    console.log('/api/news');
})

app.get('/api/modeler', function (req, res) {
     console.log('/api/modeler');
})

app.get('/api/contacts', function (req, res) {
     console.log('/api/contacts');
})

app.get('/api/about', function (req, res) {
     console.log('/api/about');
})

// =======================================================

io.on('connection', function (socket) {
    console.log('connect');
    count++;
    socket.emit('user', count);

    socket.on('stripe', function (msg, fn) {
        var charge = stripe.charges.create({
            amount: '500',
            currency: 'usd',
            source: msg.token,
            description: "Example charge"
        }, function (err, charge) {
            if (err && err.type === 'StripeCardError') {
                console.log('the card has been declined');
                console.log(err);
            }
        })
    }).on('disconnect', function (disconnect) {
        count--;
        console.log('disconnect');
        socket.emit('user', count);
    })
})

http.listen(port, function () {
    console.log('server run in ' + port);
})
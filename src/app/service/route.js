export default function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'app/template/about.html',
        controller: 'aboutCtrl'
    })
    $routeProvider.when('/contacts', {
        templateUrl: 'app/template/contacts.html',
        controller: 'contactCtrl'
    })
    $routeProvider.when('/news', {
        templateUrl: 'app/template/news.html',
        controller: 'newsCtrl'
    })
    $routeProvider.when('/modeler', {
        templateUrl: 'app/template/modeler.html',
        controller: 'modelerCtrl'
    })
    $routeProvider.otherwise({ redirectTo: '/' });
}
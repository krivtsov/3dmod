'use strict';

var app = angular.module('modeler', ['ngRoute']);

import route from './service/route.js';
import socket from './service/socket.js';
import backend from './service/service.js';

import about from'./controllers/about/about.controller.js';
import contact from'./controllers/contacts/contacts.controller.js';
import modeler from'./controllers/modeler/modeler.controller.js';
import ctrl from'./controllers/news/news.controller.js';

app.config(route);
app.factory('socket', socket);
app.service('backend', backend);

app.controller('aboutCtrl', about);
app.controller('contactCtrl', contact);
app.controller('modelerCtrl', modeler);
app.controller('newsCtrl', ctrl);





module.exports = app;



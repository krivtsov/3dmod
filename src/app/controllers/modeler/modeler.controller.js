'use strict';

export default function ($scope, backend) {
    $scope.reg_card = /^([0-9]){16}/i;
    $scope.reg_mm = /^([0-9]){2}/i;
    $scope.reg_yy = /^([0-9]){2}/i;
    $scope.reg_cvc = /^([0-9]){3}/i;
    $scope.stripe = function () {
        var cardNumber = $('#card').val();
        var expMonth = $('#mm').val();
        var expYear = $('#yy').val();
        var cvc = $('#cvc').val();
        var isValid = true;
        $('#error').text('');

        if (cardNumber === '') {
            isValid = false;
            $('#card_label').text('* Card Number');
        } else if (this.reg_card.test(cardNumber) === false) {
            isValid = false;
            $('#error').text('Card number format is incorrect.');
        }
        if (expMonth === '') {
            isValid = false;
            $('#mm_label').text('* Expiration (MM)');
        }
        if (expYear === '') {
            isValid = false;
            $('#yy_label').text('* Expiration (MM)');
        }
        if (cvc === '') {
            isValid = false;
            $('#cvc_label').text('* Expiration (MM)');
        }

        if (isValid) {
            var obj = {
                number: this.cardNumber,
                exp_month: this.expMonth,
                exp_year: this.expYear,
                cvc: this.cvc
            }
            Stripe.card.createToken(obj, stripeResponseHandler);
        } else {
            if ($('#error').text() === '')
                $('#error').text('* Please fill blank fields.');
        }
    }


    function stripeResponseHandler(status, response) {
        if (response.error) {
            console.log(response.error);
            $('#error').text('card number is incorrect');
        } else {
            console.log('token created');
            var obj = {
                number: resp.cardNumber,
                exp_month: resp.expMonth,
                exp_year: resp.expYear,
                cvc: resp.cvc,
                token: response.id
            }
            backend.buy(obj);
        }
    }
}
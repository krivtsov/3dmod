if (!Detector.webgl) Detector.addGetWebGlMessage();


var container, stats;

var blendMash, helper, camera, scene, renderer, controls;

var clock = new THREE.Clock();
var gui = null;

var isFrameStepping = false;
var timeToStep = 0;

init();

function init() {
    container = document.getElementById('container');

    scene = new THREE.Scene(); // создаем сцену
    scene.add(new THREE.AmbientLight(0xffffff));  // свет

    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: false });
    renderer.setClearColor(0x777777);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth / 2, window.innerHeight / 2);
    renderer.autoClear = true;

    container.appendChild(renderer.domElement);

    // 

    stats = new Stats();
    container.appendChild(stats.dom);

    // 

    window.addEventListener('resize', onWindowResize, false);

    // listen for messages from the gui
    window.addEventListener('start-animation', onStartAnimation);
    window.addEventListener('stop-animation', onStopAnimation);
    window.addEventListener('pause-animation', onPauseAnimation);
    window.addEventListener('step-animation', onStepAnimation);
    window.addEventListener('weight-animation', onWeightAnimation);
    window.addEventListener('crossfade', onCrossfade);
    window.addEventListener('warp', onWarp);
    window.addEventListener('toggle-show-skeleton', onShowSkeleton);
    window.addEventListener('toggle-show-model', onShowModel);

    blendMash = new THREE.BlendCharacter();
    


}
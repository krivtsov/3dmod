var gulp = require('gulp'),
    less = require('gulp-less'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer');


gulp.task('less', function () {
    return gulp.src('src/app/styles/less/**/*.*')
        .pipe(less())
        .pipe(autoprefixer(['last 15 versions', '>1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(gulp.dest('src/app/styles/css'))
        .pipe(browserSync.reload({ stream: true }))
});
gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'src'
        },
        notify: false
    })
});

gulp.task('watch', ['browser-sync', 'less'], function () {  
    gulp.watch('src/app/styles/less/*.less', ['less']);
})

gulp.task('default', ['watch']);